@Library('usta-utils-mule') _

node {
    echo "Loading ecosystem..."
    def ecosystem = utils.getEcosystem('usta-mule-ecosystem')



    def dockerEcosystem = utils.getDockerEnvironmentsMap(ecosystem)

    echo "Creating parameters..."
    def inputProps = []
    inputProps.addAll(utils.getDockerMaintenanceInputParametersList(dockerEcosystem))
    utils.generateInputFields(inputProps)

    echo "Running pipeline..."
    runner.startDockerMaintenancePipeline(params, dockerEcosystem)
}