def notifyFailure(ecosystem) {  
	echo 'Sending failed notification'			
	if (isNotificationEmailDefined(ecosystem)) {	
		emailext  recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
				subject: "Build Failure: ${currentBuild.fullDisplayName}",
				body: "Please find more details here ${env.BUILD_URL}"
	}
}

def notifySuccess(ecosystem) {  
	echo 'Sending success notification'			
	if (isNotificationEmailDefined(ecosystem)) {
		mail 	to: ecosystem.attributes.notificationEmail,
				subject: "Build Success: ${currentBuild.fullDisplayName}",
				body: "Please find more details here ${env.BUILD_URL}"
	}
}

def isNotificationEmailDefined(ecosystem) {
	return ecosystem != null && ecosystem.attributes != null && ecosystem.attributes.notificationEmail != null
}